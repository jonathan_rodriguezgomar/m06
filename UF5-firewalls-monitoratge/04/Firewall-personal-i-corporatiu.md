## Exercicis

### 1. TALLAFOCS AMB GUI PER UBUNTU

##### 1. Instal·la el programari Gufw en una màquina virtual Ubuntu Desktop (l'anomenarem "Server") i arrenca'l.
![](assets/Firewall-personal-i-corporatiu-bd718cde.png)

##### 2. En una altre equip virtual Ubuntu (serà "client") instal·la el programari zenmap (és un GUI per nmap) i arrenca'l. En cas que no puguis, pots fer servir nmap. Ens servirà per fer "escanneig" de ports i comprovar si es pot accedir als serveis del Server.
![](assets/Firewall-personal-i-corporatiu-e870aed0.png)


##### 3. Instal·la Apache i openssh-server al "Server".
![](assets/Firewall-personal-i-corporatiu-7df4f6c1.png)

##### 4. Comprova que pots veure els serveis actius al servidor des del client, accedint amb un navegador web i un client ssh.
![](assets/Firewall-personal-i-corporatiu-fb3a9877.png)
![](assets/Firewall-personal-i-corporatiu-663acd72.png)

##### 5. Comprova que Zenmap, amb l'opció "Quick scan" et mostra que els ports dels serveis web i sshd estan oberts (escoltant).
![](assets/Firewall-personal-i-corporatiu-815bcf42.png)

##### 6. Activa ara el Firewall al Server i posa l'opció Incoming a Deny i Outgoing a Allow (opcions per defecte). Des del client comprova que els ports dels serveis ja no són accessibles i tampoc es pot accedir amb els clients web i ssh.
![](assets/Firewall-personal-i-corporatiu-af6f8480.png)
![](assets/Firewall-personal-i-corporatiu-97535b6f.png)
![](assets/Firewall-personal-i-corporatiu-577bcd3b.png)

##### 7. Al "Listening report" del Firewall tenim una llista dels serveis que estan funcionant. Si escollim apache2 i piquem el botó "+" podem afegir una regla que fa referència al port on escolta apache. Volem afegir una regla per permetre que les peticions arribin al servidor apache2.
![](assets/Firewall-personal-i-corporatiu-119e4699.png)
![](assets/Firewall-personal-i-corporatiu-438f8270.png)

##### 8. Comprova que el servei ara està disponible amb Zenmap i el client web.
![](assets/Firewall-personal-i-corporatiu-788a3918.png)
![](assets/Firewall-personal-i-corporatiu-e881b86f.png)

##### 9. Fes el mateix amb el servei sshd (permetre'l), però amb l'opció per configuració ràpida d'aplicacions. pica el botó "+" del desplegable "Regles" (pestanya "preconfigurat"). Prova a afegir una regla per a permetre SSH (busca entre les categories).
![](assets/Firewall-personal-i-corporatiu-8e367575.png)
![](assets/Firewall-personal-i-corporatiu-d9e020ae.png)

##### 10. Comprova des del client que el port de ssh ara és accessible i connecta des del client SSH.
![](assets/Firewall-personal-i-corporatiu-d17f9b9b.png)
![](assets/Firewall-personal-i-corporatiu-0937c3e3.png)

---

### 2. Configuració de Pfsense

Farem servir dos equips virtuals: un amb Pfsense i un altre amb Ubuntu Desktop (pot ser la màquina creada a l'apartat anterior). També farem servir l'equip físic.

![](imgs/firewall-pfsense-3potes-12258a33.png)

##### 1. Creeu una nova màquina virtual per a instal·lar-hi el Pfsense. En les opcions de VirtualBox:
  - Activeu una interfície pont (xarxa WAN o externa pel firewall)
  - Activeu una interfície interna (xarxa LAN o interna pel Pfsense).
  - Ara Instal·leu la distribució Pfsense (mode fàcil).1
![](assets/Firewall-personal-i-corporatiu-682be805.png)
![](assets/Firewall-personal-i-corporatiu-62ff0c12.png)
![](assets/Firewall-personal-i-corporatiu-d9ac721e.png)
![](assets/Firewall-personal-i-corporatiu-2dadbce8.png)

##### 2. Apareixerà el pfsense en mode consola i us ensenyarà el menú inicial. Per defecte agafarà les interfícies de forma correcta.

~~~
*** Welcome to pfSense 2.4.4-RELEASE (amd64) on pfSense ***

 WAN (wan)       -> em0        -> v4/DHCP4: 172.19.254.240/16
 LAN (lan)       -> em1        -> v4: 192.168.1.1/24

 0) Logout (SSH only)                  9) pfTop
 1) Assign Interfaces                 10) Filter Logs
 2) Set interface(s) IP address       11) Restart webConfigurator
 3) Reset webConfigurator password    12) PHP shell + pfSense tools
 4) Reset to factory defaults         13) Update from console
 5) Reboot system                     14) Disable Secure Shell (sshd)
 6) Halt system                       15) Restore recent configuration
 7) Ping host                         16) Restart PHP-FPM
 8) Shell

Enter an option:
~~~

---

NOTA: Si fas la pràctica a casa, vigila que l'identificador de xarxa que fas servir a la teva xarxa de casa podria ser 192.168.1.0/24. Caldrà llavors canviar l'adreça IP de la interfície LAN del Pfsense (per exemple 192.168.100.0) i el servei DHCP del Pfsense per que el rang d'adreces sigui concordant. Ho pots fer amb l'opció 1 (Assign interfaces) o bé ho pots fer des de l'aplicació web anant a Services->DHCP Server->Pestanya LAN i canviant el rang.

---

##### 3. Si no s'ha fet correctament, haureu d'assignar les interfícies WAN i LAN respectivament al pfsense (opció 1).
  - Pfsense anomena les interfícies em0 i em1. Poseu WAN a la primera (és la interfície pont cap a internet) i poseu em1 a la segona (xarxa LAN interna).
  ![](assets/Firewall-personal-i-corporatiu-b1e3cd2a.png)

  - Comproveu fent ping desde la consola del pfsense (opció 7 o opció 8 escrivint ping...) que veieu per exemple el host 8.8.8.8 i www.google.es. Ja tindrem internet.
  ![](assets/Firewall-personal-i-corporatiu-bf76f354.png)

##### 4. Creeu una màquina virtual amb Ubuntu Desktop i poseu-li una interfície VBox interna a la mateixa que el Pfsense (serà la màquina de la xarxa local).
  - Comproveu que podeu fer ping a la IP interna del PfSense
    ![](assets/Firewall-personal-i-corporatiu-3ca745cb.png)
    ![](assets/Firewall-personal-i-corporatiu-58dbe1fb.png)

  - Obriu el navegador i escriviu https://ip-interna-pfsense.
    ![](assets/Firewall-personal-i-corporatiu-fd09e88e.png)

  - Així veurem la configuració via web del pfsense. Usuari: admin i contrasenya: pfsense.
    ![](assets/Firewall-personal-i-corporatiu-043e0e0e.png)

##### 5. Ara podrem configurar via web de forma remota el pfsense. Segurament us apareixerà la primera vegada el Wizard o assistent de configuració inicial. Si no, podeu anar a "System->Setup Wizard".
![](assets/Firewall-personal-i-corporatiu-043e0e0e.png)
![](assets/Firewall-personal-i-corporatiu-b4bcd670.png)
![](assets/Firewall-personal-i-corporatiu-63c69285.png)
![](assets/Firewall-personal-i-corporatiu-d19643b7.png)
![](assets/Firewall-personal-i-corporatiu-75beb7c1.png)
![](assets/Firewall-personal-i-corporatiu-b484f393.png)
![](assets/Firewall-personal-i-corporatiu-c9cdfc27.png)

---
ATENCIÓ: Aneu al menú interfaces->WAN i a la secció "Reserved Networs" desactiveu el checkbox "block private networks". Així podreu accedir a la interfície WAN des del vostres equips físics (si no, el firewall ignora qualsevol paquet provinent d'una xarxa privada cap a la interfície WAN).
![](assets/Firewall-personal-i-corporatiu-31fe387b.png)

---

##### 6. Aneu a "System->General Setup" per a comprovar la configuració del pfsense. Assegureu-vos que l'opció "Do not use the DNS Forwarder or Resolver as a DNS server for the firewall"  està marcada (si no, anirà tot més lent per que el pfsense no te activat un servidor DNS propi).
en les tres linees de dalt a la dreta
![](assets/Firewall-personal-i-corporatiu-a57bdd6a.png)
![](assets/Firewall-personal-i-corporatiu-1c818ae6.png)

##### 7. Aneu a "Diagnostics->Ping":
  - Proveu a fer ping a 8.8.8.8 (funciona enrutament).
  ![](assets/Firewall-personal-i-corporatiu-0150c62c.png)
  ![](assets/Firewall-personal-i-corporatiu-ce4eae5d.png)
  - Després "Diagnostics->DNS Lookup" i prova google.es (funciona resolució DNS).
  ![](assets/Firewall-personal-i-corporatiu-d68ade69.png)

  - Així ens assegurem que el pfsense te internet (des de la consola web).
  ![](assets/Firewall-personal-i-corporatiu-1ee362ae.png)
  ![](assets/Firewall-personal-i-corporatiu-269d6be8.png)
  ![](assets/Firewall-personal-i-corporatiu-9650010d.png)

##### 8. Si tot és correcte, ja tenim el nostre firewall corporatiu funcionant, però sense filtrar res.
  - El client es configura per DHCP ja que Pfsense activa per defecte el servei DHCP per la interfície LAN (si no ho ha fet, activa'l).
  ![](assets/Firewall-personal-i-corporatiu-67bb495c.png)

  - Comprova des del client la configuració IP (adreça, màscara, gateway i dns) i que pots accedir a alguna web.
  ![](assets/Firewall-personal-i-corporatiu-b1d05c3a.png)
  ![](assets/Firewall-personal-i-corporatiu-d49c46a4.png)

  - Comprova que Gateway i DNS apunten al Pfsense.
  ![](assets/Firewall-personal-i-corporatiu-feb22f29.png)

  - Instal·la el servei ssh al Desktop i comprova que funciona.
  ![](assets/Firewall-personal-i-corporatiu-840e8f24.png)
  ![](assets/Firewall-personal-i-corporatiu-de9a88d7.png)

  - Prova a connectar mitjançant la consola amb ftp a ftp.redhat.org. Comprova que s'hi pot accedir.
  ![](assets/Firewall-personal-i-corporatiu-0eef0c35.png)

  - Prova el servei SSH amb "ssh usuari@tty.sdf.org" (un servidor ssh a internet. Podeu crear si voleu un compte a http://sdf.org/?signup i entrar amb el vostre usuari). Comprova que funciona.
  ![](assets/Firewall-personal-i-corporatiu-3bc81428.png)




---
NOTA: Si el servei DNS del Pfsense no funciona, haureu de posar al client l'adreça IP del servidor DNS que fa servir el vostre equip físic. Comproveu amb nslookup des del client que la resolució de noms funciona.  

---
#### Política PERMISSIVA al Firewall, blocar el que no volem

##### 9. Apliquem amb la política oberta (tot permés, apliquem restriccions). Posarem restriccions de certs protocols. Bloca els ICMP de la xarxa LAN interna. Això es fa a "Firewall->Rules".
  - A la pestanya LAN (interfície LAN pel pfsense), afegirem una regla nova que bloqui el protocol ICMP (origen LAN Net, que és qualsevol adreça de la xarxa LAN i destí qualsevol).
  - Comprova que no pots fer ping a 8.8.8.8 (ni cap màquina pública).
  - Bloca ara el servei web segur i no segur. Són dues regles, una per a cada port (origen LAN Net, destinació qualsevol, port destí 80 http, port destí 443 https). Comprova que no pots navegar per pàgines web. Però si pots fer servir ssh i la resta de serveis.
  - Canvia les regles anteriors per posar Reject en lloc de Block. Quina diferència trobes quan intentes navegar per la pàgina http o https? Explica-ho. Per un atacant, quina és pitjor?

---

NOTA: Pot aparèixer una campana a la barra superior del Pfsense indicant que hi ha avisos. Si veiem que algun d'ells parla de que no es pot assignar memòria, aneu a "System > Advanced > Firewall & NAT > Firewall Maximum Table Entries". Veureu el valor per defecte de 200.000 entrades. Poseu 300.000 i apliqueu els canvis. S'hauria de solucionar.

---

#### Política RESTRICTIVA al Firewall, permetre el que volem

##### 10. Ara mirarem de blocar tot el tràfic de la xarxa interna cap a internet, per configurar una política restrictiva (tot tancat, obrim excepcions).
  - Desactiva (sense eliminar) totes les regles de la interfície LAN. Pfsense sempre treballa en mode restrictiu, i per tant l'última regla, que s'ha d'assumir, és blocar tot.
  - Pots desactivar les regles que havíem posat abans picant a sobre la icona de desactivar a la dreta de la regla.
  - Les regles es tornen grises quan es desactiven.
  - També les pots esborrar si estàs segur que no les necessites més. Comprovem de nou els serveis i ens assegurem que no funcionen.

##### 11. Obrirem l'accés de la xarxa interna cap a serveis web i web segura. Has de posar les regles abans de la que bloca tots els protocols.
  - Comprovem que es pot accedir a serveis web. Recorda que s'ha de poder resoldre el nom de domini (DNS, port 53 TCP i UDP)...
  - Prova ftp i ssh. (La resta de protocols continuarà sense funcionar).

##### 12. Obrirem l'accés a ssh. Recorda posar-la abans de la que bloca tots els protocols.
  - Comprovem que es pot accedir a un servidor ssh a internet (ssh tty.sdf.org).
  - Instal·la ssh a la teva màquina física i connectat amb ssh des del client virtual. Comprova amb Wireshark, des de la màquina física quines IP's són les que realment fan la petició ssh. Hauria de ser la IP de la interfície WAN del nostre PFsense? o la del nostre client Ubuntu?
