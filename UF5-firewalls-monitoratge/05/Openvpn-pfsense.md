## Referències i ajuda

- Configurar VPN a Pfsense. Vídeo Pau Tomé - https://www.youtube.com/watch?v=K26Pir7xu5s
- Descàrrega Pfsense - https://www.pfsense.org/download/
- Documentació Pfsense - https://docs.netgate.com/pfsense/en/latest/index.html
- Configurar VPN amb Pfsense - http://www.3ops.com/implementacion-de-vpn-cliente-servidor-con-openvpn-y-pfsense/

### Introducció

![](imgs/openvpn-pfsense-5a523c6f.png)

Per a crear un servidor OpenVPN i poder connectar a ell es necessita:
- Un servidor OpenVPN, que serà el Pfsense. La interfície amb IP pública per connectar-se serà la WAN (que en la simulació en realitat serà una IP privada de l'aula).
- Al menys un client que es connecta via la interfície WAN del servidor.
- Un certificat per al servidor VPN (amb les seves corresponents claus privada i pública).
- Una CA (Certificate Authority, recordeu la infrastructura PKI) en que puguin confiar els clients i el servidor.
- Un certificat per a cada client (amb les seves corresponents claus privada i pública).
- Configurar fitxer de OpenVPN del servidor i el client amb els mateixos paràmetres.

Llavors seguirem les següents passes per a configurar la VPN amb PFSense:
1. Creació de Certificate Authority (CA) i Certificat Arrel de la CA.
2. Creació del Certificat del Servidor.
3. Configuració del Servidor OpenVPN.
4. Activar una regla per obrir el port VPN al Firewall (a la interfície WAN).
5. Creació dels usuaris + Certificats dels usuaris.
6. Exportació del fitxer de configuració del client i instal·lació del Client.

Servidor i unitat de certificacio:
![](assets/Openvpn-pfsense-2dceb19c.png)
![](assets/Openvpn-pfsense-8a5c132d.png)
![](assets/Openvpn-pfsense-598b819e.png)
![](assets/Openvpn-pfsense-1685fbc7.png)
![](assets/Openvpn-pfsense-ae788c53.png)
![](assets/Openvpn-pfsense-19b4645f.png)
![](assets/Openvpn-pfsense-b0ecbc8c.png)
![](assets/Openvpn-pfsense-3cb9b181.png)
![](assets/Openvpn-pfsense-9c40b806.png)
![](assets/Openvpn-pfsense-d4ca380f.png)

Crear usuaris:
![](assets/Openvpn-pfsense-a4155f06.png) user manager
![](assets/Openvpn-pfsense-5e628936.png) add
![](assets/Openvpn-pfsense-cae81ff6.png)
![](assets/Openvpn-pfsense-df68e487.png)
![](assets/Openvpn-pfsense-15ecaa2b.png)
![](assets/Openvpn-pfsense-5eef62d6.png)

**L'objectiu serà poder treballar a la xarxa interna de l'empresa (darrera el firewall Pfsense) des de fora però com si fòssim a la mateixa xarxa local.**

Posarem un equip virtual connectat en xarxa interna a la interfície LAN del pfsense. Hi posarem un servidor ssh amb un Banner on aparegui el nostre nom i un servidor web amb una pàgina que mostri el nostre nom i cognoms.

Blocarem tot el tràfic de la LAN cap a fora (cal treure les regles permissives) i de la WAN cap a la LAN (no hauríem de fer res), per assegurar-nos que només s'hi pot accedir pel tunel VPN. Només es permetrà al Firewall el port de la VPN.

---

### Configuració de servidor OpenVPN amb Wizard

Podem fer servir el Wizard per a crear un nou servidor OpenVPN o fer les passes manualment com hem descrit abans.

- Anem a VPN->OpenVPN->Wizard

En el cas d'executar amb Wizard, instal·lem un servidor d'accès remot. Podem escollir desprès entre Remote Access (servidor-client, per treballadors remots) o Peer to peer (entre servidors si volem connectar dues seus):

1. **Type of Server**: Escollim on resideixen els usuaris (base de dades local, LDAP o RADIUS). Per no haver de muntar un altre servei, escollim local.
![](assets/Openvpn-pfsense-5a2ec4fe.png)

2. **Escollir la CA**. Si no l'hem creat abans, podem fer-ne una de nova ara. Poseu el vostre nom-cognom per identificar-la.
![](assets/Openvpn-pfsense-2a43244c.png)

3. Crear el **certificat del servidor VPN**. Si no l'hem creat abans, podem fer-ne un de nou ara.
![](assets/Openvpn-pfsense-82ad689c.png)

4. Ara cal configurar els **paràmetres del servidor OpenVPN**

- **Interfície** (WAN per donar accés a equips remots, altres per a accès local)
- **Protocol** (UDP per defecte, per tenir velocitat en xarxes fiables. TCP per tenir fiabilitat a costa de velocitat)
- **Port**, 1194 per defecte en OpenVPN.
- **Descripció**: Les nostres dades.
![](assets/Openvpn-pfsense-857dd2f3.png)

---

- **Paràmetres criptogràfics**:
1. Autenticació TLS i creació de nova clau TLS.
2. Paràmetres de la clau Diffie Hellman (DH), usats per establir una comunicació segura.
3. Algorisme de xifrat entre els dos extrems (server i clients l'han de configurar igual). Pot ser accelerat per hardware si les màquines disposen de xips criptogràfics (AES per defecte).
4. Algorisme hash d'autenticació (per defecte SHA256)
5. Hardware d'acceleració criptogràfica si n'hi ha.
![](assets/Openvpn-pfsense-1f8c2b4e.png)

---

- **Paràmetres del tunnel**:
1. **Xarxa a fer servir al tunel** (posarem una xarxa privada que no es faci servir enlloc més, per exemple 10.0.8.0/24. El servidor tindrà al seu extrem del tunel l'adreça 10.0.8.1 i els clients aniran agafant adreces succesives).
2. **Redirect Gateway**: Força a tots els equips a passar TOT el seu tràfic pel tunel.
3. **Local Network**: Xarxes locals que hi ha darrera el servidor VPN. S'hi podrà accedir des dels nodes remots a les que s'indica aquí.
4. Connexions concurrents màximes
5. Compressió de les dades (carrega els equips però aprofita millor ample de banda)
6. Type-of-Service
7. Inter-Client Communication, els clients es veuran o no.
8. Allow multiple concurrent connections: permetre obrir més d'una connexió VPN amb el mateix nom (no recomanat). Permet mateix certificat per diferents hosts.
![](assets/Openvpn-pfsense-772a7baa.png)

---

- **Opcions de client**:
1. IP dinàmica
2. Topology
3. DNS default Domain, DNS Server 1, 2, 3, 4, NTP Server,
4. NetBIOS Options, NetBIOS Node Type, NetBIOS Scope ID
5. WINS Server 1, 2
![](assets/Openvpn-pfsense-31ed60d2.png)

---

- **Regles de firewall**
1. Permetre accedir al port 1194 des de internet
2. Permetre passar pel tunel el tràfic VPN.
![](assets/Openvpn-pfsense-58513837.png)

---

### Revisió dels paràmetres

1. Podem revisar les configuracions del servidor OpenVPN a VPN->OpenVPN.
![](assets/Openvpn-pfsense-f7ba8285.png)

2. Podem revisar la configuració de la CA i els certificats a System->Cert Manager i a la pestanya CA o Certificates.
![](assets/Openvpn-pfsense-5b4cc18d.png)

3. Podem crear usuaris LOCALS per utilitzar a la VPN a System->User Manager. A la pestanya "Authentication Servers" podríem enllaçar amb un servidor extern d'autenticació LDAP o RADIUS.
![](assets/Openvpn-pfsense-45c858df.png)

4. Podem revisar les regles del firewall. D'entrada observeu que tenim una interfície virtual nova (de tunel) anomenada per defecte OpenVPN. Veurem les regles que s'hi ha afegit.
![](assets/Openvpn-pfsense-9f52faf5.png)

---

### Depuració

Si alguna cosa va malament podem fer servir diferents eines:

Al servidor
- Posar un panell de visualització a la pantalla principal (Dashboard, picar +, afegir "OpenVPN").
![](assets/Openvpn-pfsense-ab34746b.png)

- Logs del Firewall (Firewall->Rules->icones a dalt a la dreta - status i logs)
![](assets/Openvpn-pfsense-0c709d5c.png)
![](assets/Openvpn-pfsense-0c709d5c.png)
![](assets/Openvpn-pfsense-a56affc5.png)

- Logs de VPN (VPN->OpenVPN->icones a dalt a la dreta - status i logs)
![](assets/Openvpn-pfsense-5e385063.png)
![](assets/Openvpn-pfsense-065c0c71.png)

Als clients
- Logs de client OpenVPN Gnome:
~~~
tail -f /var/log/syslog | grep vpn
~~~

- Logs de client Windows via l'app del client.

---

### Crear usuari i certificat: System->User Manager

Pestanya Users:
1. Crear nou usuari que es connectarà via client VPN (a la base de dades interna del Pfsense).
![](assets/Openvpn-pfsense-e744fb39.png)
2. Crear el certificat de client (vigilar que el tipus de certificat no sigui servidor si no de client).
![](assets/Openvpn-pfsense-46e39789.png)

---

### Exportar el fitxer de configuració del client:

Configurar el client VPN (són els equips que volen inciar sessió a la VPN des d'internet, per exemple):

A l’equip que vol iniciar sessió VPN (per a provar podeu fer servir l’equip físic **que s'ha de trobar a la xarxa EXTERNA**):
1. Exportar el fitxer de configuració OpenVPN del Pfsense (VPN->OpenVPN->Client Export). Si no hi és l'opció, instal·leu el paquet de Pfsense que dona la funcionalitat.
(El error que no hem deixaba descarregar el paqet era perque hi ha que fer un update pero amb la opcio 13 de pfsense)

2. Vigileu de marcar l’opció Legacy client, si el client openvpn de l’equip client és anterior a la versió 2.4. En cas contrari no cal.

3. Baixeu la configuració adequada per al tipus d’equip client. Aquí baixem "Inline Configurations", "Most clients". Obtindrem un fitxer amb extensió ovpn. Aquest fitxer és la configuració per un client OpenVPN. Veureu que hi ha dues parts:
- La primera conté les directives de configuració del client OpenVPN.
- La segona conté els certificats i claus.
- El fitxer que baixeu conté la configuració del client VPN semblant a aquesta:

~~~
dev tun
persist-tun
persist-key
data-ciphers-fallback AES-128-CBC
auth SHA256
tls-client
client
resolv-retry infinite
remote 192.168.0.28 1194 udp4
lport 0
verify-x509-name "Server-Pau-Tome" name
auth-user-pass
remote-cert-tls server

<ca>
-----BEGIN CERTIFICATE-----
MIIEGDCCAwCgAwIBAgIBADANBgkqhkiG9w0BAQsFADBmMRQwEgYDVQQDFAtDQS1Q
YXUtVG9t6TELMAkGA1UEBhMCRVMxEjAQBgNVBAgTCUNhdGFsdW55YTETMBEGA1UE
BxMKR3Jhbm9sbGVyczEYMBYGA1UEChMPUGF1LWVudGVycHJpc2VzMB4XDTIwMTIw
MTEyMDkzM1oXDTMwMTEyOTEyMDkzM1owZjEUMBIGA1UEAxQLQ0EtUGF1LVRvbekx
CzAJBgNVBAYTAkVTMRIwEAYDVQQIEwlDYXRhbHVueWExEzARBgNVBAcTCkdyYW5v
bGxlcnMxGDAWBgNVBAoTD1BhdS1lbnRlcnByaXNlczCCASIwDQYJKoZIhvcNAQEB
BQADggEPADCCAQoCggEBALwcKqrVuVoIbGQnrYC/Cois1zepPigfvBIO7yPXKhJs
7ESwKLgbwA94r2LfdeiwYy47BP+gl83vUFFnnsozMP00EVM34ztNW+7NrK4h0z7x
i3VndFBnOfp5mYEr8B+w5da8CfiVBE/uC1SPDToVefHhJTTag7A3dMjv4FCxlPEe
278/pPZFsmrtwJeU6TVjXg8NsLZUsn0zbjwAHtf5ajl0zpzVSnUsAT4UR600s29z
Ny/UwVTYDdGmFQynfnuS5NUUVvP/XAavMbfIENM+f8qrte1A/kzv6d4Xmova7o+L
Lh6cLL1cqgGITyo1UbcG7z3Sa90uEHraOLfzIY+CX0ECAwEAAaOB0DCBzTAdBgNV
HQ4EFgQUfITfx9IiWj8RpLOLqTh0CjhqbggwgZAGA1UdIwSBiDCBhYAUfITfx9Ii
Wj8RpLOLqTh0CjhqbgihaqRoMGYxFDASBgNVBAMUC0NBLVBhdS1Ub23pMQswCQYD
VQQGEwJFUzESMBAGA1UECBMJQ2F0YWx1bnlhMRMwEQYDVQQHEwpHcmFub2xsZXJz
MRgwFgYDVQQKEw9QYXUtZW50ZXJwcmlzZXOCAQAwDAYDVR0TBAUwAwEB/zALBgNV
HQ8EBAMCAQYwDQYJKoZIhvcNAQELBQADggEBAIH2GeQwj2IV0dzPbucAE2u9y9h3
5PbcKWqCwBCDwUjG/7/hA+v/lmm9XQ+OM7aEGeTuGpnuwJ5GvsEQENI7mCwxzsv/
MTCVTf/BSZOEPP06U9YDDKCmG+nf3BB5iYoSNVPDdh1h5ezjhBRW/yc6TCFIZnIW
CXQdLPs9dYNkgA6/xc/ZZPJkv5N0GRZEt+DU1HWRO5gu0QMakhPXZ3Y/wiVBY5c/
yePGzLH6rYzp00D/xGSaFyWVfD54YL6NeAFtv3k5pHDYETBIXfHcPVg7kHcYKPX3
RmXHYdCktz9DKsZ1Kl5uALTflAq7dJOc05n5I2PMgUzkCbAxY7wJDLJmC4U=
-----END CERTIFICATE-----
</ca>
<cert>
-----BEGIN CERTIFICATE-----
MIIEhTCCA22gAwIBAgIBAjANBgkqhkiG9w0BAQsFADBmMRQwEgYDVQQDFAtDQS1Q
YXUtVG9t6TELMAkGA1UEBhMCRVMxEjAQBgNVBAgTCUNhdGFsdW55YTETMBEGA1UE
BxMKR3Jhbm9sbGVyczEYMBYGA1UEChMPUGF1LWVudGVycHJpc2VzMB4XDTIwMTIw
MjExMzU1NloXDTMwMTEzMDExMzU1NlowbTEbMBkGA1UEAxMSdXN1YXJpUEFVLmFj
bWUuY29tMQswCQYDVQQGEwJFUzESMBAGA1UECBMJQ2F0YWx1bnlhMRMwEQYDVQQH
EwpHcmFub2xsZXJzMRgwFgYDVQQKEw9QYXUtZW50ZXJwcmlzZXMwggEiMA0GCSqG
SIb3DQEBAQUAA4IBDwAwggEKAoIBAQDIpkfOZCsySeMzUhnsSMKyJG0IE0AsLnes
4Z9BU3xoI1xF3cDqVdBMcp5yRpgzLadwFhf4nnJkXN+mJhrgoZJW0VP9xZIN5nB8
pZYl9FbIOC2XQyOD/YGVLzFNDqKZ+ehMer68r4BysH/pC1sV1Bqz3HVChJqQZ6tD
sNNAQiyXgBkdj2ISII8sLKgqhiwWc8fUUsCpthzDl3a2WzD9MlgcX6QzIH9H8KX5
uNLt9GWrGuamXnKIYN3zh5ku49siEaDdgk6biIhI5N9BbnKFJzYsBZWQ+SBJIPmN
euk5A/I9ijMuQKnTGuC3forBfeYqpgdqVPFvuI3/I8tFnAHVK4MtAgMBAAGjggE1
MIIBMTAJBgNVHRMEAjAAMAsGA1UdDwQEAwIF4DAxBglghkgBhvhCAQ0EJBYiT3Bl
blNTTCBHZW5lcmF0ZWQgVXNlciBDZXJ0aWZpY2F0ZTAdBgNVHQ4EFgQUnGe7jnTV
f9jPJF8m57ucTQluTm8wgZAGA1UdIwSBiDCBhYAUfITfx9IiWj8RpLOLqTh0Cjhq
bgihaqRoMGYxFDASBgNVBAMUC0NBLVBhdS1Ub23pMQswCQYDVQQGEwJFUzESMBAG
A1UECBMJQ2F0YWx1bnlhMRMwEQYDVQQHEwpHcmFub2xsZXJzMRgwFgYDVQQKEw9Q
YXUtZW50ZXJwcmlzZXOCAQAwEwYDVR0lBAwwCgYIKwYBBQUHAwIwHQYDVR0RBBYw
FIISdXN1YXJpUEFVLmFjbWUuY29tMA0GCSqGSIb3DQEBCwUAA4IBAQBhBhgBE8SJ
msiuHGUtuzCCflkzM3s3EPWEQDsqjj+X2wYRbo9XLLFgaJylzNAe5+684vavka3J
imElgIHe6o3S2A3deqQVik111N3cdXPUXI7SU/5aS5AE8keYeAKJAP/d533hWoXv
cwmuKvLl8RlbN+6fEiO4ky9uj11gBTE0ak9ZaEAQj1j0NawJZc7kKhNk5zjGvWje
UO/JeRgauHNJOVUIzOceK2+VqwwmmTqJ5EFrHP7Sk5ED1IF9DCe6M2NqohKGjseD
Yw3VUcdTka/PBqPRK5q4+K7qGdQOeMGxg6c1uNisNwiuIEVIXlYuTDkIR3LOg3dS
EmoIGpr2SdXw
-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDIpkfOZCsySeMz
UhnsSMKyJG0IE0AsLnes4Z9BU3xoI1xF3cDqVdBMcp5yRpgzLadwFhf4nnJkXN+m
JhrgoZJW0VP9xZIN5nB8pZYl9FbIOC2XQyOD/YGVLzFNDqKZ+ehMer68r4BysH/p
C1sV1Bqz3HVChJqQZ6tDsNNAQiyXgBkdj2ISII8sLKgqhiwWc8fUUsCpthzDl3a2
WzD9MlgcX6QzIH9H8KX5uNLt9GWrGuamXnKIYN3zh5ku49siEaDdgk6biIhI5N9B
bnKFJzYsBZWQ+SBJIPmNeuk5A/I9ijMuQKnTGuC3forBfeYqpgdqVPFvuI3/I8tF
nAHVK4MtAgMBAAECggEBAJDbiye0JYNu2HpqoZB2jykxJw4XZ9L90SSc1u7AqSY9
sRY3nFkRjwpCDaYO1T7NjP5c1P2G1qsiacx34hueShonke26P1GNtlQm75mDJ0vx
UujLbc6SrITZx0wMbtOfGlhsnVt3kNI8HnxO2HmP4Z9MX9OgDD6IuZprBHh1zd+p
MjgTuZgJgPos6mzPlyzcVY80JevGc+9Obfvr6MnYbNfkIWs5hyev+zf2w3n2EGjz
biJJWX8a8/5uDRXBZXJv/GDPn79W6Xu+WumFwtKyr4bXcdNKJlFghQaBW8Cvk2ZJ
TKCeVFgQCa+dojm8jUlIlxXDcAfJbr7JmIvthJfxP4ECgYEA45j4BrU5AKa95t6u
ZMhqZ+honENRnSiY7SNqYiIKEIxRqagAQnVuKobxT7SvWxx63euH3P9SoWWKUOhH
LGPKkIWEO7maTMlVOVi277XC42ObIJ5pr7CvPATZkDMYVU+NAI5NQ06jiG4KAhFZ
/VnqNq48hX1uz9GIrwFWXLMEMl0CgYEA4bBoNc2q5dGM5mdv6mKWxk3324eUXNwH
SOzubGp1IPulTsp6ydFKo6kii2lZ72lpVleF3nq8j+SaXgRULx4U8iXePAJZA0Rk
GXs+pVEcFv9kyy27PyUFAWvPnkpIQ8NAetM6VDovw2BpMa1dpX8vUfYHqRoXfyHG
vt7VJ5OYJxECgYBqSQxCrayiBjwWrZUJUaKHRbxchP9/Ae9whqJuk3DZy31zWhZM
8uPwjjXwv/NH/CPQsOYwqTdgUbfnEqHyM+Pr274l2gYZp8fZ0PkVNm99mw1djTzT
pBIhWC51rbbGWDEDunCQZw/tbV3VG19H9g5vXKy8thfiivpmp5w6DAuArQKBgGZA
4+ZEqcJ4f+tAQD+krYVpe/wV4I12D0LmPlwWm7z25v3c1WctETE3h3Q8pkfHrbc1
7mxPlbFoNNyV/4JdkcXojFkKA0RsryGwakDCkASHcCZyMN06K5tqOmYQZ89z3noH
y+yvdEaEHwUn9D7wcgMjgnMGZFDwf7ipA1BsGcrRAoGAafG2PZh7nuszjlDXAKdx
X7Dm4z3nYTxHiwRlIzKzyssbUwAVGlOWSJVnMdIGwPZ5KiolCOITP9y0VZlSsQZT
VIB/RR+IOvTVNDHw+zLA4P7/nRFNLUV/i6B6UHKyA9SSwYbdrFxuEeox8haVS3CT
JWAhmPnd7OQjnf53uXb3NIU=
-----END PRIVATE KEY-----
</key>
key-direction 1
<tls-auth>
#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
e40585fd2e64bbdc7663e0cb7c9c0a19
eb7d2ff614d15474314c53d8017feb68
d25b6b1a7fe4fbdba2d2f6f22642996f
2a1bc30fff23063966486f9ff1a9339d
9e3257c50fdd9c1626dfe4fde3a2035c
ba57ab5fe4beb47704fab871de78fe7f
e86807cca17a35da3f62ee804abb5535
cce4079a662cce950f328d4d0b06a32b
506d96fe75cd9d38dd21a0b256a11c45
3c5d8928b3b03bec9983227a78396cbf
d8371d52cf460385a66dbeb78ab3b66d
ef017875e2706e841688fc478b6c9c4f
243b30a635ab350b9edb345e32aaa2a2
9119b5a4f72e46b39a71c08458a4f662
f1c93af0561ee7daf3e3801ae1d9bc8a
901317bf1527b9e9bf3d8c60e794e99a
-----END OpenVPN Static key V1-----
</tls-auth>
~~~

Observeu que hi ha uns blocs marcats:

~~~
<cert> ... </cert>
<key> ... </key>
<ca> ... </ca>
<tls-auth>...</tls-auth>
~~~

Aquests blocs contindran respectivament:
- el certificat de l’usuari amb la seva clau pública (cert)
- la clau privada de l’usuari (key)
- el certificat de la CA (ca) que havíem creat.
- La clau TLS per a identificar clients i servidors (és opcional però aporta un nivell més de seguretat).

En principi, podríem guardar-ho en fitxers independents i incloure'ls al fitxer ovpn (no farem res d'aixà ara).

Si executeu directament a consola el client VPN hauria de funcionar la connexió (instal·leu el client OpenVPN si no hi és):
~~~
sudo openvpn --config config.ovpn
~~~

Captureu el resultat i mostreu la nova interfície creada a l'equip client. Mostreu també els logs on indiquin que l'equip s'ha connectat.

---











### Exercicis

#### Client en mode gràfic (Linux i Windows)

##### Linux

**Recordeu que el client s'ha de trobar a la XARXA EXTERNA.**

1. Instal·leu al client el paquet network-manager-openvpn-gnome si és necessari.
   ![](assets/Openvpn-pfsense-3e9bec26.png)

2. A la icona de Network Manager->Edita les connexions->Afegeix->VPN->OpenVPN.
![](assets/Openvpn-pfsense-c21b5f37.png)
![](assets/Openvpn-pfsense-54da3bea.png)

3. A la icona de Network Manager -> Edita les connexions -> Afegeix (+) -> VPN -> Importa de fitxer. Si no funciona, llavors opció OpenVPN i configurar manualment.


4. Per provar la connexió aneu al Network Manager->Conexions VPN i escolliu la connexió creada.


5. Comproveu que s’ha creat una interfície tun amb la ip de la xarxa virtual assignada (normalment 10.0.8.2).


6. Feu una captura dels logs, tant si funciona bé com si no:
- Al client: sudo tail -f /var/log/syslog | grep vpn
- Al servidor: VPN->OpenVPN->icona “Related logs entries” a la part superior dreta de la pantalla. O bé, Status->OpenVPN.


7. Si tot va bé, haureu de poder fer ping a un equip de la xarxa local que hi ha darrera del servidor VPN (usant la seva adreça privada) i també al servidor VPN en la seva adreça per defecte, 10.0.8.1 (si heu fet servir la xarxa 10.0.8.0 per les adreces virtuals de la VPN).


8. Obriu el navegador per accedir al servidor web del client de la xarxa interna, amb la seva adreça privada. Connecteu també via ssh al servidor intern.


9. Vigileu que el firewall permeti passar els protocols que feu servir per a les proves (Firewall->Rules->OpenVPN).


10. Comproveu amb traceroute des del client remot per quins gateways es passa per arribar a Internet.


---

#### Windows

1. Instal·leu el client openVPN per Windows i feu servir el mateix fitxer de configuració per iniciar sessió a la VPN.

2. Comproveu de la mateixa manera que amb Linux.
