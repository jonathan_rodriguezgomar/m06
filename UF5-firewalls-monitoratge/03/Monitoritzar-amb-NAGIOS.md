## Exercicis

##### 1. Segueix els passos per la instal·lació de Nagios XI en una màquina Ubuntu Server. La instal·lació és llarga i triga una estona.
No he fet la instalacio de la maquina virtual perque he iportat la ova.
![](assets/Monitoritzar-amb-NAGIOS-7f91ae10.png)
![](assets/Monitoritzar-amb-NAGIOS-960ebae3.png)
![](assets/Monitoritzar-amb-NAGIOS-d7b48434.png)
![](assets/Monitoritzar-amb-NAGIOS-c9eee992.png)
Ja ho tinc instalat
![](assets/Monitoritzar-amb-NAGIOS-dba300a8.png)

##### 3.  Configura un 'dispositiu de xarxa genèric' i fes servir l'adreça del servidor web que hi ha a la xarxa. Demostra que si el servidor s'atura, Nagios detecta que no pot fer ping i envia una notificació via correu electrònic.
Despres de configurar-ho amb la ip del server:
![](assets/Monitoritzar-amb-NAGIOS-b6d3e5bf.png)
Veiem que esta OK:
![](assets/Monitoritzar-amb-NAGIOS-ebd52573.png)
I despres de parar el server
![](assets/Monitoritzar-amb-NAGIOS-d23c1381.png)
![](assets/Monitoritzar-amb-NAGIOS-55e84805.png)

##### 4.  Busca quina configuració pot ser la més adient per monitorar un servei web i com podem validar que el servei està actiu i funcionant correctament.
hi ha un apartat que es diu: ejecutar un asistent de configuració hon pots seleccionar diversos tipus de serveis per ejemple:
![](assets/Monitoritzar-amb-NAGIOS-c13d9fd1.png)
per validar aquest servei primer tens que donarli clic i ficar el domini que vols monitoritzar:
![](assets/Monitoritzar-amb-NAGIOS-78e86c79.png)
Luego en este caso la ip del servidor DNS:
![](assets/Monitoritzar-amb-NAGIOS-78e86c79.png)
![](assets/Monitoritzar-amb-NAGIOS-4f6164a8.png)

##### 5. Verifica que aparegui un text determinat dins de la web i si no apareix que envii una notificació.
Ja he configurado para que me mande un correo cada vez que se pare el servicio:
![](assets/Monitoritzar-amb-NAGIOS-79cce194.png)

##### 6. Instal·la un agent al servidor web. fes servir l'agent ncpa i segueix les instruccions d'instal·lació del mateix Wizard de configuració.Tingues en compte que cal tenir python instal·lat a l'equip on volem desplegar l'agent i sortida a internet (afegeix al servidor una targeta NAT per tenir-ho). Després cal desplegar una sonda NCPA per obtenir els paràmetres interns del servidor (CPU, usuaris, espai en disc, etc.).
1. Entrem en configuració i cliquem en: Deploy Agent
2. Despres fiquem els parametres adecuats en el meu cas aquests:
![](assets/Monitoritzar-amb-NAGIOS-5499006c.png)
3. Cliquem en el boto que diu deploy i esperem
4. Cuan finalitze surt aixo:
![](assets/Monitoritzar-amb-NAGIOS-3e84260e.png)
5. Run Wizard i ja tinguem tots els parametres:
![](assets/Monitoritzar-amb-NAGIOS-3db3deb7.png)
![](assets/Monitoritzar-amb-NAGIOS-b5c5b49e.png)
